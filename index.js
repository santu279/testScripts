var express = require('express');
var _ = require('lodash');
var lowdb = require('lowdb');
var storage = require('lowdb/file-async');
var uuid = require('uuid');
var bodyParser = require('body-parser');

var app = express();

app.db = lowdb('db.json', {
   storage:storage 
});

app.use(bodyParser.json());

app.get('/tasks', function(req,res){
    return res.json(app.db('tasks'));
});

app.get('/tasks/:id', function(req,res){
    var id = req.params.id;
    var tasks = app.db('tasks').find({
        id: id
    });
    if(task){
        return res.json(task);
    }
    return res.status(404).end();
});

//addig new task

app.post('/tasks/:id', function(req, res){
    var task = req.body;
    task.id = uuid();
    app.db('tasks').push(task)
    return res.status(201).end();
})

//update a task

app.put('/tasks/:id', function(req, res){
    var id = req.params.id;
    var task = req.body;
    app.db('tasks')
        .chain()
        .find({
            id:id
        })
        .assign(task)
        .value()
    return res.status(201).end();
})

//delete the task

app.delete('/tasks/:id', function(req, res){
    var id = req.params.id;
    app.db('tasks').remove({
        id: id
    });
    return res.status(201).end();
})

app.listen(3000, function(){
    console.log('API UP and running');
});

module.exports = app;