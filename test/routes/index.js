import { request } from "https";

describe('Task API Routes', function(){
    beforeEach(function(done) {
        app.db.object = {};
        app.db.object.tasks = [{
            id:uuid(),
            title: 'study',
            done: false
        }, {
            id: uuid(),
            title:'work',
            done:true
        }];
        app.db.write();
        done();
    });

    describe('GET /tasks', function() {
        it('returns a list of tasks', function(done) {
            request.get('/tasks')
                .expect(200)
                .end(function(err, res) {
                    expect(res.body).to.have.lengthOf(2);
                    done(err);
                });
        });
    });
    //testing the save task expecting status 201 of success

    describe('POST /tasks', function(){
        it('saves a new task', function(done){
            request.post('/tasks')
                .send({
                    title: 'run',
                    done: false
                })
                .expect(201)
                .end(function(err, res) {
                    done(err);
                });
        });
    });

    describe('GET /tasks/:id', function() {
        it('return a task by id', function(done) {
            var task = app.db('tasks').first();
            request.get('/tasks/' + task.id)
                .expect(200)
                .end(function(err, res){
                    expect(res.body).to.eql(task);
                    done(err);
                });
        });
    //testing the status 404 for task not found
    it('return status 404 when id is not found', function(done) {
        var task = {
            id: 'fakeId'
        }
        request.get('/tasks' + task.id)
            .expect(404)
            .end(function(err, res){
                done(err);
            });
    });
});

//testing how to update a task expecting status 201 of success

describe('PUT /tasks/:id', function() {
    it('update a task', function(done){
        var task = app.db('tasks').first();
        request.put('/tasks' + task.id)
            .send({
                title: 'travel',
                done: false
            })
            .expect(201)
            .end(function(err, res){
                done(err);
            });
    });
})

describe('DELETE /tasks/:id', function(){
    it('remove a task', function(done){
        var task = app.db('tasks').first();
        request.put('/tasks/' + task.id)
            .expect(201)
            .end(function(err, res){
                done(err);
            });
    });
});
});